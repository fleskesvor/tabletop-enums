package com.fleskesvor.tabletop

import kotlin.js.JsName

@JsName("type")
enum class Type(val type: String, val description: String) {
    BOARDGAME("BOARDGAME", "A game played using a gameboard"),
    CARDS("CARDS", "A game played using cards"),
    DICE("DICE", "A game played using dice")
}