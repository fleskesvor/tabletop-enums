package com.fleskesvor.tabletop.card

import kotlin.js.JsName

@JsName("suit")
enum class Suit(val type: String, val description: String) {
    CLUBS("CLUBS", "Clubs"),
    DIAMONDS("DIAMONDS", "Diamonds"),
    HEARTS("HEARTS", "Hearts"),
    SPADES("SPADES", "Spades")
}