const enums = require("./tabletop-enums.js");

const MAX_DEPTH = 5;

const enumToObjectArray = (array) => {
   return array.values().map((value) => {
      return {type: value.type, description: value.description};
   });
};

const enumsToLiterals = (enums, counter = 1) => {
   if (counter > MAX_DEPTH) {
      console.error(`Max depth ${MAX_DEPTH} reached!`);
      return {};
   }

   if (typeof enums === "function") {
      return enumToObjectArray(enums);
   }

   return Object.keys(enums).reduce((literals, key) => {
      literals[key] = enumsToLiterals(enums[key], counter+1);
      return literals
   }, {});
};

module.exports = {
   enums,
   literals: enumsToLiterals(enums)
};
