import org.jetbrains.kotlin.gradle.targets.js.webpack.KotlinWebpackConfig.Mode
import org.jetbrains.kotlin.gradle.targets.js.webpack.KotlinWebpackOutput.Target

plugins {
    id("org.jetbrains.kotlin.multiplatform") version "1.3.70"
    // FIXME: Problems with dceTask.keep and "ReferenceError: window is not defined"
    //id("nebula.release") version "13.0.0"
}
repositories {
    maven { setUrl("https://dl.bintray.com/kotlin/kotlin-eap") }
    mavenCentral()
}
group = "com.fleskesvor"
version = "0.0.1" // FIXME: Set dynamically from git tag/commit

apply(plugin = "maven-publish")

kotlin {
    jvm()
    js {
        nodejs {
        }
        browser {
            @Suppress("EXPERIMENTAL_API_USAGE")
            dceTask {
                keep("tabletop-enums.com.fleskesvor.tabletop")
            }
	        testTask { enabled = false }
            webpackTask {
                output.libraryTarget = Target.COMMONJS
                output.library = "" // don't append project name to exported library
                mode = Mode.DEVELOPMENT // TODO: Change to PRODUCTION in CI pipeline
            }
        }
    }
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        jvm().compilations["main"].defaultSourceSet {
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
            }
        }
        jvm().compilations["test"].defaultSourceSet {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
            }
        }
        js().compilations["main"].defaultSourceSet {
            dependencies {
                implementation(kotlin("stdlib-js"))
            }
        }
        js().compilations["test"].defaultSourceSet {
            dependencies {
                implementation(kotlin("test-js"))
            }
        }
    }
}

tasks {
    register("buildNpm") {
        doLast {
            // Overwrite package.json with dynamically set version
            copy {
                from ("$projectDir/src/jsMain/resources/package.json") {
                    filter { it.replace("0.0.1", "${rootProject.version}") }
                }
                into("$buildDir/distributions")
            }
        }
    }
    register("publishToNpmLocal") {
        doLast {
            exec {
                workingDir = File("$buildDir/distributions")
                commandLine("npm", "link")
            }
        }
    }
    named("buildNpm") {
        dependsOn("jsJar", "processDceJsKotlinJs")
    }
    named("publishToNpmLocal") {
        dependsOn("buildNpm")
    }
    named("build") {
        dependsOn("buildNpm")
    }
}
